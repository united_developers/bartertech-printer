package com.ad.bartertechprinter.ui.main.inputtransaction

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.injection.factory.ViewModelKey
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class InputTransactionModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun inputTransactionFragment(): InputTransactionFragment

    @Binds
    @IntoMap
    @ViewModelKey(InputTransactionViewModel::class)
    abstract fun bindInputTransactionViewModel(viewModel: InputTransactionViewModel): ViewModel
}