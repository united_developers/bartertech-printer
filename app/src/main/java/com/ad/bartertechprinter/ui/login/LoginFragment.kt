package com.ad.bartertechprinter.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.databinding.FragmentLoginBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class LoginFragment : DaggerFragment() {

    private lateinit var binding: FragmentLoginBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

//    private lateinit var navigationViewModel: MainNavigationViewModel
    private lateinit var viewModel: LoginViewModel
    lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        initiateViewModels()
        return binding.root
    }

    private fun initiateViewModels() {
//        navigationViewModel =
//            ViewModelProvider(requireActivity(), viewModelFactory).get(MainNavigationViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(LoginViewModel::class.java)

        binding.viewModel = viewModel
        binding.handler = this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
        // TODO: Use the ViewModel
    }


    fun onLoginClick() {

    }
}
