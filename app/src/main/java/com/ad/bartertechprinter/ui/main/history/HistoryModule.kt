package com.ad.bartertechprinter.ui.main.history

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.injection.factory.ViewModelKey
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


@Module
abstract class HistoryModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun historyFragment(): HistoryFragment

    @Binds
    @IntoMap
    @ViewModelKey(HistoryViewModel::class)
    abstract fun bindHistoryViewModel(viewModel: HistoryViewModel): ViewModel
}