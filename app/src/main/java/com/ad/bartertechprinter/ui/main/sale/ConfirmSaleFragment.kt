package com.ad.bartertechprinter.ui.main.sale

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.databinding.FragmentConfirmSaleBinding
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import com.ad.bartertechprinter.ui.main.home.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@FragmentScoped
class ConfirmSaleFragment : DaggerFragment() {

    private lateinit var binding: FragmentConfirmSaleBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: SaleViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sale, container, false)
        initiateViewModels()
        return binding.root
    }

    private fun initiateViewModels() {
        homeViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(HomeViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(
            SaleViewModel::class.java)

        binding.homeViewModel = homeViewModel
        binding.viewModel = viewModel
        binding.handler = this
    }

    fun onDoneClick() {
        viewModel.makeSale()
    }

}