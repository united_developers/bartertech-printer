package com.ad.bartertechprinter.ui.main.home

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.injection.data.BartertechPrinterRepository
import com.ad.bartertechprinter.injection.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class HomeViewModel @Inject constructor(
    private val repository: BartertechPrinterRepository
) : ViewModel(){

    private lateinit var homeNavigator: HomeNavigator

    fun popBackStack() {
        homeNavigator.popBackStack()
    }

    fun setNavigator(navigator: HomeNavigator) {
        homeNavigator = navigator
    }

    private fun navigateTo(navigation: Int) {
        homeNavigator.navigate(navigation)
    }

    fun navigateToHistoryFragment() {
        navigateTo(R.id.action_homeFragment_to_historyFragment)
    }

    fun navigateToInputTransactionFragment() {
        navigateTo(R.id.action_homeFragment_to_inputTransactionFragment)
    }

    fun navigateToSettingsFragment() {
        navigateTo(R.id.action_homeFragment_to_settingsFragment)
    }

    fun navigateToSaleFragment() {
        navigateTo(R.id.action_homeFragment_to_saleFragment)
    }

    fun navigateToConfirmFragment() {
        navigateTo(R.id.action_saleFragment_to_confirmSaleFragment)
    }
}