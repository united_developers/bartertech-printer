package com.ad.bartertechprinter.ui.main.history

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.injection.data.BartertechPrinterRepository
import com.ad.bartertechprinter.injection.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class HistoryViewModel @Inject constructor(
    private val repository: BartertechPrinterRepository
) : ViewModel(){
}