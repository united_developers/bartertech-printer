package com.ad.bartertechprinter.ui.main.home


interface HomeNavigator {

    fun popBackStack()

    fun navigate(navigateAction: Int)
}