package com.ad.bartertechprinter.ui.main.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ad.bartertechprinter.R
import com.ad.bartertechprinter.databinding.FragmentHistoryBinding
import com.ad.bartertechprinter.injection.scopes.FragmentScoped
import com.ad.bartertechprinter.ui.main.home.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@FragmentScoped
class HistoryFragment : DaggerFragment() {

    private lateinit var binding: FragmentHistoryBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: HistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false)
        initiateViewModels()
        return binding.root
    }

    private fun initiateViewModels() {
        homeViewModel =
            ViewModelProvider(requireActivity(), viewModelFactory).get(HomeViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity(), viewModelFactory).get(HistoryViewModel::class.java)

        binding.homeViewModel = homeViewModel
        binding.viewModel = viewModel
        binding.handler = this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initiateTagListAdapter()
    }


    private fun initiateTagListAdapter() {

    }
}