package com.ad.bartertechprinter.ui.main.inputtransaction

import androidx.lifecycle.ViewModel
import com.ad.bartertechprinter.injection.data.BartertechPrinterRepository
import com.ad.bartertechprinter.injection.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class InputTransactionViewModel @Inject constructor(
    private val repository: BartertechPrinterRepository
)  : ViewModel() {
}