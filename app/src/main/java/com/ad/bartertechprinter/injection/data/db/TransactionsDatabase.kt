package com.ad.bartertechprinter.injection.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ad.bartertechprinter.injection.data.db.models.Transaction

@Database(entities = [Transaction::class], version = 1,
    exportSchema = false)
abstract class TransactionsDatabase: RoomDatabase() {

    abstract fun transactionsDao(): TransactionsDao

    companion object {

        private val DATABASE_NAME = "transactions-db"
        fun getInstance(context: Context): TransactionsDatabase =
            buildDatabase(
                context
            )
        /**
         * Set up the database configuration.
         * The SQLite database is only created when it's accessed for the first time.
         */
        private fun buildDatabase(appContext: Context): TransactionsDatabase {
            return Room.databaseBuilder(appContext, TransactionsDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}
object SubscriptionDbProvider {

    private var subscriptionDatabase: TransactionsDatabase? = null

    fun getInstance(context: Context): TransactionsDatabase {
        if (subscriptionDatabase == null) {
            subscriptionDatabase =
                TransactionsDatabase.getInstance(
                    context
                )
        }
        return subscriptionDatabase!!
    }

}