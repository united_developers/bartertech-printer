package com.ad.bartertechprinter.injection.data

import android.content.SharedPreferences
import com.ad.bartertechprinter.injection.data.db.TransactionsDao
import com.ad.bartertechprinter.injection.scopes.AppScoped
import javax.inject.Inject

@AppScoped
class BartertechPrinterData @Inject constructor(
    private val transactionsDao: TransactionsDao,
    private val sharedPreferences: SharedPreferences): BartertechPrinterRepository{
}