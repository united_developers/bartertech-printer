package com.ad.bartertechprinter.injection.component

import android.app.Application
import com.ad.bartertechprinter.app.BartertechPrinterApp
import com.ad.bartertechprinter.injection.factory.ViewModelBuilder
import com.ad.bartertechprinter.injection.module.ActivityBindingModule
import com.ad.bartertechprinter.injection.module.AppModule
import com.ad.bartertechprinter.injection.module.BartertechPrinterRepositoryModule
import com.ad.bartertechprinter.injection.scopes.AppScoped
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

@AppScoped
@Component(
    modules = [AndroidInjectionModule::class,
        AppModule::class,
        ViewModelBuilder::class,
        ActivityBindingModule::class,
        BartertechPrinterRepositoryModule::class]
)
interface AppComponent : AndroidInjector<BartertechPrinterApp> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}