package com.ad.bartertechprinter.injection.factory

import androidx.lifecycle.ViewModelProvider
import com.ad.bartertechprinter.injection.factory.DaggerAwareViewModelFactory
import dagger.Binds
import dagger.Module


@Module
internal abstract class ViewModelBuilder {

    @Binds
    internal abstract fun bindViewModelFactory(factory: DaggerAwareViewModelFactory):
            ViewModelProvider.Factory
}