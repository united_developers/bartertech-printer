package com.ad.bartertechprinter.injection.module

import com.ad.bartertechprinter.injection.scopes.ActivityScoped
import com.ad.bartertechprinter.ui.login.LoginActivity
import com.ad.bartertechprinter.ui.login.LoginModule
import com.ad.bartertechprinter.ui.main.MainActivity
import com.ad.bartertechprinter.ui.main.history.HistoryModule
import com.ad.bartertechprinter.ui.main.home.HomeModule
import com.ad.bartertechprinter.ui.main.inputtransaction.InputTransactionModule
import com.ad.bartertechprinter.ui.main.sale.SaleModule
import com.ad.bartertechprinter.ui.main.settings.SettingsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [HomeModule::class,
            HistoryModule::class,
            SettingsModule::class,
            SaleModule::class,
            InputTransactionModule::class
        ]

    )
    internal abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [LoginModule::class
        ]
    )
    internal abstract fun loginActivity(): LoginActivity


}