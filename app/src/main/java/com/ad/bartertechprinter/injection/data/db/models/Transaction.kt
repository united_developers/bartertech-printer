package com.ad.bartertechprinter.injection.data.db.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Transaction(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "transaction_id")
    var transactionId: Long = 0,


    @ColumnInfo(name = "time_stamp")
    var timeStamp: Boolean = true
)