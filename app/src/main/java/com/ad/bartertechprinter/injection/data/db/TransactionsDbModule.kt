package com.ad.bartertechprinter.injection.data.db

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class TransactionsDbModule {

    @Provides
    fun provideTransactionsDb(context: Context): TransactionsDatabase {
        return TransactionsDatabase.getInstance(context)
    }

    @Provides
    fun provideTransactionsDao(transactionsDb: TransactionsDatabase): TransactionsDao {
        return transactionsDb.transactionsDao()
    }

}