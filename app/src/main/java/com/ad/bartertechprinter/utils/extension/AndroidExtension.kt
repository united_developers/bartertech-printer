package com.ad.bartertechprinter.utils.extension

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.ad.bartertechprinter.utils.SimpleChooseDialogHelper
import com.google.android.material.snackbar.Snackbar

inline fun Activity.showSimpleEnterAlertDialog(layoutId: Int, func: SimpleChooseDialogHelper.() -> Unit): AlertDialog =
    SimpleChooseDialogHelper(this, layoutId).apply {
        func()
    }.create()

val Context.isConnected: Boolean
    get() {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            .activeNetworkInfo?.isConnected == true
    }

fun Fragment.showMessage(message: String) {
    val messageSnack = Snackbar.make(this.requireView(), message, Snackbar.LENGTH_SHORT)
    messageSnack.show()
}

fun Fragment.showMessage(messageId: Int) {
    val messageSnack = Snackbar.make(this.requireView(), messageId, Snackbar.LENGTH_SHORT)
    messageSnack.show()
}

fun Fragment.showToast(message: String) {
    Toast.makeText(this.requireContext(), message, Toast.LENGTH_SHORT).show()
}

fun Fragment.showToast(messageId: Int) {
    Toast.makeText(this.requireContext(), messageId, Toast.LENGTH_SHORT).show()
}