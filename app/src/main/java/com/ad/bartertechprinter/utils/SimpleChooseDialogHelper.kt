package com.ad.bartertechprinter.utils

import android.content.Context
import android.text.Html.FROM_HTML_MODE_LEGACY
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat
import android.text.InputFilter
import android.widget.ImageView
import android.widget.RadioGroup
import com.ad.bartertechprinter.R


class SimpleChooseDialogHelper(
    val context: Context,
    layoutId: Int
) : BaseDialogHelper() {

    //  dialog view
    override val dialogView: View by lazy {
        LayoutInflater.from(context).inflate(layoutId, null)
    }

    override val builder: AlertDialog.Builder = AlertDialog.Builder(context).setView(dialogView)
//
//
//    //  done icon
//    private val confirmClick: TextView by lazy {
//        dialogView.findViewById<TextView>(R.id.dialog_confirm_btn)
//    }
//
//    private val contentView: TextView by lazy {
//        dialogView.findViewById<TextView>(R.id.dialog_content_view)
//    }
//
//    private val contentImage: ImageView by lazy {
//        dialogView.findViewById<ImageView>(R.id.dialog_image)
//    }
//
//    private val enterFirstValue: EditText by lazy {
//        dialogView.findViewById<EditText>(R.id.enter_first_value)
//    }
//
//    private val enterSecondValue: EditText by lazy {
//        dialogView.findViewById<EditText>(R.id.enter_second_value)
//    }
//
//    fun setContentResourceImage(resource: Int) {
//        contentImage.setImageResource(resource)
//    }
//
//    private val dialogTitle: TextView by lazy {
//        dialogView.findViewById<TextView>(R.id.dialog_title)
//    }
//
//    //  close icon
//    private val cancelClick: TextView by lazy {
//        dialogView.findViewById<TextView>(R.id.dialog_cancel_btn)
//    }
//
//    fun getEnterET(): EditText {
//        return enterFirstValue
//    }
//
//    //  closeIconClickListener with listener
//    fun cancelClickListener(func: (() -> Unit)? = null) =
//        with(cancelClick) {
//            setClickListenerToDialogButton(true, func)
//        }
//
//    fun getEnterValue(): String {
//        return enterFirstValue.text.toString()
//    }
//
//    fun updateContent(text: String) {
//        contentView.text = HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_COMPACT)
//    }
//
//    fun updateContent(text: Int) {
//        contentView.setText(text)
//    }
//
//
//    fun getSecondEnterValue(): String {
//        return enterSecondValue.text.toString()
//    }
//
//    fun dialogUpdateTitle(text: String) {
//        dialogTitle.text = HtmlCompat.fromHtml(text, FROM_HTML_MODE_LEGACY)
//    }
//
//    fun dialogUpdateHint(text: String) {
//        enterFirstValue.hint = text
//    }
//
//    fun updateMaxLines(count: Int) {
//        enterFirstValue.maxLines = count
//    }
//
//    fun updateLength(count: Int) {
//        enterFirstValue.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(count))
//    }
//
//    fun updateTextFirstValue(text: String) {
//        enterFirstValue.setText(text)
//    }
//
//    fun updateTextSecondValue(text: String) {
//        enterSecondValue.setText(text)
//    }
//
//    fun setTextColor(resId: Int) {
//        enterFirstValue.setTextColor(resId)
//    }
//
//    fun updateConfirmText(resId:Int) {
//        confirmClick.setText(resId)
//    }
//
//    fun updateConfirmText(text:String) {
//        confirmClick.text = text
//    }
//
//    //  doneIconClickListener with listener
//    fun confirmClickListener(func: (() -> Unit)? = null) =
//        with(confirmClick) {
//            setClickListenerToDialogButton(true, func)
//        }
//
//    fun sortCheckListener(func: ((id: Int) -> Unit)? = null) =
//        with (radioGroup) {
//            setOnCheckedChangeListener { _, id ->
//                func?.invoke(id)
//                dialog?.dismiss()
//            }
//        }
//
//    fun confirmClickWithParameterListener(dismissOnClick: Boolean, func: (() -> Unit)? = null) =
//        with(confirmClick) {
//            setClickListenerToDialogButton(dismissOnClick, func)
//        }
//
//    //  view click listener as extension function
//    private fun View.setClickListenerToDialogButton(dismissOnClick: Boolean, func: (() -> Unit)?) =
//        setOnClickListener {
//            func?.invoke()
//            if (dismissOnClick)
//                dialog?.dismiss()
//        }
}