package com.ad.bartertech.network

import android.content.SharedPreferences
import com.ad.bartertechprinter.utils.TOKEN
import com.ad.bartertechprinter.utils.extension.get
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TokenInterceptor @Inject constructor(var sharedPreferences: SharedPreferences) : Interceptor {

    var token: String = ""

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()

        val originalHttpUrl = original.url()
        val requestBuilder = original.newBuilder()
            .header("Accept", "application/json")
            .method(original.method(), original.body())
            .url(originalHttpUrl)
        val path = original.url().encodedPath()
        if (!path.contains("auth") && !path.contains("forgotPassword")) {
            token = sharedPreferences.get(TOKEN, "")
            requestBuilder.header("Authorization", "Bearer $token")
        }


        // Request customization: add request headers


        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}