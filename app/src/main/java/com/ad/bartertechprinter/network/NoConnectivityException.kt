package com.ad.bartertechprinter.network

import java.io.IOException


class NoConnectivityException(override var message: String) : IOException(message){

}