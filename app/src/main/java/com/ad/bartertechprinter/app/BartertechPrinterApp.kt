package com.ad.bartertechprinter.app

import androidx.lifecycle.ProcessLifecycleOwner
import com.ad.bartertechprinter.injection.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

class BartertechPrinterApp  : DaggerApplication() {

//    @Inject
//    lateinit var lifecycleListener: ForegroundBackgroundListener

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
//        ProcessLifecycleOwner.get().lifecycle
//            .addObserver(lifecycleListener)
    }

}